#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        for line in self.rfile:
            # Leyendo línea a línea lo que nos envía el cliente
            print(line.decode('utf-8'))
            x = line.decode('utf-8').find(" ")
            y = line.decode('utf-8').find(":")
            z = line.decode('utf-8').find("@")
            s = line.decode('utf-8').find("SIP/2.0")

            if x == -1 or y == -1 or z == -1 or s == -1:
                if line.decode('utf-8') != "\r\n":
                    self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
            else:
                MetodoSIP = line.decode('utf-8')[:x]
                Receptor = line[y+1:z]
                IP_receptor = line[z+1:s-1]
                Peticion_valida = (
                    MetodoSIP + " sip:" + Receptor.decode('utf-8') +
                    "@" + IP_receptor.decode('utf-8') + " SIP/2.0\r\n")
                if Peticion_valida != line.decode('utf-8'):
                    self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")

                else:
                    if MetodoSIP == "INVITE":
                        self.wfile.write(
                            b"SIP/2.0 100 Trying\r\n" +
                            b"SIP/2.0 180 Ringing\r\n" +
                            b"SIP/2.0 200 OK\r\n" +
                            b"Content-Type: application/sdp\r\n\r\n" +
                            b"v=0\r\n" + b"o=" + Receptor + b"@kpop.com " +
                            IP_receptor + b"\r\ns=Album\r\n" +
                            b"t=0\r\nm=audio " +
                            bytes(str(PUERTO_SERVER), 'utf-8') +
                            b" RTP\r\n")
                    elif MetodoSIP == "ACK":
                        BIT = secrets.randbits(1)
                        RTP_header = simplertp.RtpHeader()
                        RTP_header.set_header(
                            version=2, marker=BIT, payload_type=14,
                            ssrc=200002)
                        audio = simplertp.RtpPayloadMp3(Fichero_audio)
                        simplertp.send_rtp_packet(RTP_header, audio,
                                                  "127.0.0.1", 23032)
                    elif MetodoSIP == "BYE":
                        self.wfile.write(b"SIP/2.0 200 OK\r\n")
                    else:
                        if line.decode('utf-8') != "\r\n":
                            self.wfile.write(
                                b"SIP/2.0 405 Method Not Allowed\r\n")


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos\

    if len(sys.argv) != 4:
        sys.exit("Usage: python3 server.py IP port audio_file")

    IP_SERVER = sys.argv[1]
    try:
        PUERTO_SERVER = int(sys.argv[2])
    except ValueError:
        sys.exit("Usage: python3 server.py IP port audio_file")

    Fichero_audio = sys.argv[3]

    try:
        with open(Fichero_audio, 'r') as f_a:
            print("Listening...")
    except FileNotFoundError:
        sys.exit("Usage: python3 server.py IP port audio_file")
    serv = socketserver.UDPServer((IP_SERVER, PUERTO_SERVER), EchoHandler)
    serv.serve_forever()
