#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys


if len(sys.argv) != 3:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")

MetodoSIP = sys.argv[1]
Info_receptor = sys.argv[2]

x = Info_receptor.find("@")
y = Info_receptor.find(":")
if x == -1 or y == -1:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")

try:
    PuertoSIP = int(Info_receptor[y+1:])
except ValueError:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")

Receptor = Info_receptor[:x]
IP_receptor = Info_receptor[x+1:y]
Mitad_mensaje = (
    Receptor + "@" + IP_receptor +
    " SIP/2.0" + "\r\n")
Mensaje_ACK = "ACK sip:" + Mitad_mensaje
Mensaje_SIP = MetodoSIP + " sip:" + Mitad_mensaje

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP_receptor, PuertoSIP))

    my_socket.send(bytes(Mensaje_SIP, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido --\r\n' + data.decode('utf-8'))

    d_dec = data.decode('utf-8')

    if MetodoSIP == "INVITE" and d_dec != "SIP/2.0 400 Bad Request\r\n":
        my_socket.send(bytes(Mensaje_ACK, 'utf-8') + b'\r\n')
